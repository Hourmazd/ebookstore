using eBook.Base.API.Controllers;
using eBook.Base.Common;
using eBook.Base.Common.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Warehouse.API.Controllers
{
    [Route("")]
    [ApiController]
    public class HomeController : HomeControllerBase
    {
        public HomeController(IOptions<AppOptions> options, IServiceId serviceId)
            : base(options, serviceId)
        {
        }
    }
}