﻿using Gateway.Application.ViewModels;
using RestEase;
using System.Threading.Tasks;

namespace Gateway.Application.Forwarders
{
    public interface IWarehouseForwarder
    {
        [AllowAnyStatusCode]
        [Get("api/warehouse/get/{id}")]
        Task<BookDTO> Get([Path] int id);
    }
}
