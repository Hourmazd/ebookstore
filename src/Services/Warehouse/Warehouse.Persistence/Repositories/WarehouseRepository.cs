﻿using Warehouse.Domain.Entities;
using eBook.Base.Persistence.Repositories;
using MediatR;

namespace Warehouse.Persistence.Repositories
{
    public class WarehouseRepository : RepositoryBase<WarehouseDbContext, WarehouseEntity>, IWarehouseRepository
    {
        public WarehouseRepository(WarehouseDbContext context, IMediator mediator)
            : base(context, mediator)
        {
        }
    }
}
