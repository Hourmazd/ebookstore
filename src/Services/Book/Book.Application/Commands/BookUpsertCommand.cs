﻿using Book.Application.IntegrationEvents.Events;
using Book.Domain.Entities;
using Book.Domain.Events;
using Book.Persistence.Repositories;
using eBook.Base.Application.Commands;
using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Types;
using eBook.Base.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Book.Application.Commands
{
    public class BookUpsertCommand : UpsertCommandBase
    {
        public BookUpsertCommand(ICorrelationContext context = null)
            : base(context) { }

        public string ISBN { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public decimal Price { get; set; }
    }

    public class BookUpsertCommandHandler
        : UpsertCommandHandlerBase<BookEntity, BookUpsertCommand, IBookRepository>
    {
        public BookUpsertCommandHandler(IBookRepository repository, IBusPublisher busPublisher)
            : base(repository, busPublisher)
        {
        }

        protected override async Task<BookEntity> Complete(BookUpsertCommand command, BookEntity entity, CancellationToken cancellationToken)
        {
            var priceChanged = entity != null && entity.Price != command.Price;

            if (entity is null)
                entity = new BookEntity();

            entity.SetProperty(e => e.ISBN, command.ISBN);
            entity.SetProperty(e => e.Title, command.Title);
            entity.SetProperty(e => e.Author, command.Author);
            entity.SetProperty(e => e.Price, command.Price);

            if (entity.IsTransient)
                entity.AddDomainEvent(new BookCreatedDomainEvent(entity));
            else if (entity.IsDirty)
                entity.AddDomainEvent(new BookUpdatedDomainEvent(entity));

            if (priceChanged)
            {
                var priceChangedEvent = new BookPriceChangedIntegrationEvent(entity.Id, entity.Price, 0);
                await _BusPublisher.PublishAsync(priceChangedEvent, command.Context);
            }

            return entity;
        }
    }
}
