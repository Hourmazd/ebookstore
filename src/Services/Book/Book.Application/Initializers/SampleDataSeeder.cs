﻿using Book.Application.Commands;
using Book.Application.Queries;
using MediatR;
using System.Threading.Tasks;

namespace Book.Application.Initializers
{
    public class SampleDataSeeder
    {
        private readonly IMediator _Mediator;

        public SampleDataSeeder(IMediator mediator)
        {
            _Mediator = mediator;
        }

        public async Task SeedData()
        {
            for (int i = 1; i <= 10; i++)
            {
                var query = new BookSingleQuery() { EntityId = i };
                var entity = await _Mediator.Send(query);

                if (entity is null)
                {
                    var command = new BookUpsertCommand()
                    {
                        Id = i,
                        ISBN = i.ToString("00000"),
                        Author = "Author " + i,
                        Title = "Title " + i,
                        Price = i * 10
                    };

                    await _Mediator.Send(command);
                }
            }
        }
    }
}
