﻿using Warehouse.Domain.Entities;
using Warehouse.Domain.Events;
using Warehouse.Persistence.Repositories;
using eBook.Base.Application.Commands;
using eBook.Base.Common.RabbitMq;
using System.Threading;
using System.Threading.Tasks;

namespace Warehouse.Application.Commands
{
    public class WarehouseDeleteCommand : DeleteCommandBase
    {
        public WarehouseDeleteCommand(ICorrelationContext context = null)
            : base(context) { }
    }

    public class BookDeleteCommandHandler
        : DeleteCommandHandlerBase<WarehouseEntity, WarehouseDeleteCommand, IWarehouseRepository>
    {
        public BookDeleteCommandHandler(IWarehouseRepository repository, IBusPublisher busPublisher)
            : base(repository, busPublisher)
        {
        }

        protected override Task<WarehouseEntity> Complete(WarehouseDeleteCommand command, WarehouseEntity entity, CancellationToken cancellationToken)
        {
            if (entity != null)
                entity.AddDomainEvent(new WarehouseDeletedDomainEvent(command.Id));

            return Task.FromResult(entity);
        }
    }
}
