﻿using eBook.Base.Common.Logging;
using eBook.Base.Common.Redis;
using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Extentions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace eBook.Base.Common.HealthCheck
{
    public static class Extentions
    {
        public static IServiceCollection AddDefaultHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            var hcBuilder = services.AddHealthChecks();

            hcBuilder.AddCheck("self", () => HealthCheckResult.Healthy());

            var redisOptions = configuration.GetOptions<RedisOptions>("redis");
            if (redisOptions.Enabled)
                hcBuilder
                    .AddRedis(
                        redisOptions.ConnectionString,
                        name: "redis-check",
                        tags: new string[] { "redis" });

            var rabbitOptions = configuration.GetOptions<RabbitMqOptions>("rabbitMq");
            if (rabbitOptions.Enabled)
                foreach (var host in rabbitOptions.Hostnames)
                    hcBuilder
                        .AddRabbitMQ(
                            $"amqp://{host}",
                            name: "rabbitmq-check",
                            tags: new string[] { "rabbitmqbus" });

            var elastiOptions = configuration.GetOptions<ElasticSearchOptions>("elasticsearch");
            if (elastiOptions.Enabled)
                hcBuilder.AddElasticsearch(
                    elastiOptions.Url,
                    name: "elastic-check",
                    tags: new string[] { "elastic-search" });


            //if (configuration.GetValue<bool>("AzureServiceBusEnabled"))
            //{
            //    hcBuilder
            //        .AddAzureServiceBusTopic(
            //            configuration["EventBusConnection"],
            //            topicName: "eshop_event_bus",
            //            name: "basket-servicebus-check",
            //            tags: new string[] { "servicebus" });
            //}

            return services;
        }
    }
}
