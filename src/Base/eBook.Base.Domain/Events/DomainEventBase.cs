﻿using eBook.Base.Domain.Entities;
using MediatR;

namespace eBook.Base.Domain.Events
{
    public abstract class DomainEventBase : INotification
    {
        protected DomainEventBase(int entityId)
        {
            EntityId = entityId;
        }

        public int EntityId { get; }
    }

    public abstract class DomainEventBase<TEntity> : DomainEventBase
        where TEntity : EntityBase
    {
        protected DomainEventBase(TEntity entity)
            : base(entity.Id)
        {
            Entity = entity;
        }

        public TEntity Entity { get; }
    }
}
