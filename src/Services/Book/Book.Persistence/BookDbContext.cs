﻿using Book.Domain.Entities;
using Book.Persistence.EntityConfiguration;
using eBook.Base.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Book.Persistence
{
    public class BookDbContext : DbContextBase, IBookDbContext
    {
        #region Constructors

        public BookDbContext(DbContextOptions<BookDbContext> options)
            : base(options)
        {
            System.Diagnostics.Debug.WriteLine($"BookDbContext::ctor {GetHashCode()}");
        }

        #endregion

        #region Public Properties

        public const string DEFAULT_SCHEMA = "dbo";

        #endregion

        #region DbSets

        public DbSet<BookEntity> Books { get; set; }

        #endregion

        #region Override Members

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new BookEntityTypeConfiguration());
        }

        #endregion
    }
}
