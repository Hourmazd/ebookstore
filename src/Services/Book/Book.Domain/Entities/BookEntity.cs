﻿using eBook.Base.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace Book.Domain.Entities
{
    public class BookEntity : EntityBase
    {
        #region Constructors

        public BookEntity() : base()
        {
        }

        #endregion

        #region Private Fields
        #endregion

        #region Properties

        [Required]
        public string ISBN { get; protected set; }

        [Required]
        public string Title { get; protected set; }

        [Required]
        public string Author { get; protected set; }

        [Required]
        public decimal Price { get; protected set; }

        #endregion

        #region Public Methods
        #endregion
    }
}
