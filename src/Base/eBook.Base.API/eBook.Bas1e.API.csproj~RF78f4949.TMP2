<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>netcoreapp3.1</TargetFramework>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="AspNetCore.HealthChecks.Elasticsearch" Version="3.0.1" />
    <PackageReference Include="AspNetCore.HealthChecks.Rabbitmq" Version="3.0.7" />
    <PackageReference Include="AspNetCore.HealthChecks.Redis" Version="3.0.0" />
    <PackageReference Include="AspNetCore.HealthChecks.UI.Client" Version="3.0.2" />
    <PackageReference Include="Microsoft.AspNetCore.HttpOverrides" Version="2.2.0" />
    <PackageReference Include="Microsoft.Extensions.Hosting.Abstractions" Version="3.1.3" />
    <PackageReference Include="Microsoft.Extensions.Configuration.Binder" Version="3.1.3" />
    <PackageReference Include="Microsoft.Extensions.Caching.Redis" Version="2.2.0" />

    <PackageReference Include="Autofac" Version="5.1.2" />
    <PackageReference Include="Autofac.Extensions.DependencyInjection" Version="6.0.0" />

    <PackageReference Include="Serilog" Version="2.9.0" />
    <PackageReference Include="Serilog.AspNetCore" Version="3.2.0" />
    <PackageReference Include="Serilog.Extensions.Logging" Version="3.0.1" />
    <PackageReference Include="Serilog.Sinks.Console" Version="3.1.1" />
    <PackageReference Include="Serilog.Sinks.Elasticsearch" Version="8.0.1" />
    <PackageReference Include="Serilog.Sinks.Seq" Version="4.0.0" />

    <PackageReference Include="App.Metrics.Core" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Core" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Endpoints" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Health" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Health.Endpoints" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Hosting" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Mvc" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Reporting" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Routing" Version="3.2.0" />
    <PackageReference Include="App.Metrics.AspNetCore.Tracking" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Extensions.Configuration" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Extensions.DependencyInjection" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Formatters.InfluxDB" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Formatters.Json" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Formatters.Prometheus" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Health.Checks.Http" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Reporting.Console" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Reporting.Http" Version="3.2.0" />
    <PackageReference Include="App.Metrics.Reporting.InfluxDB" Version="3.2.0" />

    <PackageReference Include="Swashbuckle.AspNetCore" Version="5.3.1" />
    <PackageReference Include="Swashbuckle.AspNetCore.Annotations" Version="5.3.1" />
    <PackageReference Include="Swashbuckle.AspNetCore.Filters" Version="5.1.0" />
    <PackageReference Include="Swashbuckle.AspNetCore.ReDoc" Version="5.3.1" />
    <PackageReference Include="Swashbuckle.AspNetCore.Swagger" Version="5.3.1" />
    <PackageReference Include="Swashbuckle.AspNetCore.SwaggerGen" Version="5.3.1" />
    <PackageReference Include="Swashbuckle.AspNetCore.SwaggerUI" Version="5.3.1" />
    <PackageReference Include="System.IdentityModel.Tokens.Jwt" Version="6.5.0" />

    <PackageReference Include="Jaeger" Version="0.3.7" />
    <PackageReference Include="OpenTracing.Contrib.NetCore" Version="0.6.2" />

    <PackageReference Include="RawRabbit" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.DependencyInjection.ServiceCollection" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.Enrichers.Attributes" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.Enrichers.Polly" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.Enrichers.MessageContext" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.Enrichers.MessageContext.Subscribe" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.DependencyInjection.Autofac" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.Enrichers.RetryLater" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.Operations.Publish" Version="2.0.0-rc5" />
    <PackageReference Include="RawRabbit.Operations.Subscribe" Version="2.0.0-rc5" />
  </ItemGroup>

  <ItemGroup Condition="'$(Configuration)' == 'Debug'">
    <ProjectReference Include="..\eBook.Base.Common\eBook.Base.Common.csproj" />
    <ProjectReference Include="..\eBook.Base.Domain\eBook.Base.Domain.csproj" />
    <ProjectReference Include="..\eBook.Base.Application\eBook.Base.Application.csproj" />
  </ItemGroup>
  <ItemGroup Condition="'$(Configuration)' == 'Release'">
    <PackageReference Include="eBookShop.Base.Common" Version="1.0.0" />
    <PackageReference Include="eBookShop.Base.Domain" Version="1.0.0" />
    <PackageReference Include="eBookShop.Base.Application" Version="1.0.0" />
  </ItemGroup>

</Project>
