﻿using Warehouse.Domain.Entities;
using eBook.Base.Persistence.Types;

namespace Warehouse.Application.Queries
{
    public class WarehouseDataFetchSpecification : DataFetchSpecification<WarehouseEntity>
    {
    }
}
