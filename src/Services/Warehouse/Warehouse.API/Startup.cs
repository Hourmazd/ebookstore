using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Warehouse.Domain;
using Warehouse.Application;
using Warehouse.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using eBook.Base.Common.Types;
using Warehouse.Application.IntegrationEvents.Events;
using Consul;
using eBook.Base.API.Extentions;
using eBook.Base.Common.Mvc;
using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Consul;

namespace Warehouse.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public ILifetimeScope AutofacContainer { get; private set; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDomain();
            services.AddApplication(Configuration);
            services.AddPersistence(Configuration);

            services.AddDefaultServices(Configuration);
            services.AddInitializers(typeof(ISqlDbInitializer));

            services.AddControllers();
        }

        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IHostApplicationLifetime applicationLifetime,
            IConsulClient client,
            IStartupInitializer startupInitializer)
        {
            AutofacContainer = app.ApplicationServices.GetAutofacRoot();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultApplicationSettings();
            app.UseRabbitMq()
                .SubscribeEvent<BookPriceChangedIntegrationEvent>(@namespace: "bookService", onError: (@event, exp) =>
                {
                    return new BookPriceChangedRejectedEvent(@event.Id, @event.BookId, exp.Message, exp.Code);
                });

            var consulServiceId = app.UseConsul();
            applicationLifetime.ApplicationStopped.Register(() =>
            {
                client.Agent.ServiceDeregister(consulServiceId);
                AutofacContainer.Dispose();
            });

            startupInitializer.InitializeAsync();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder
                .RegisterAssemblyTypes(Assembly.GetEntryAssembly())
                .AsImplementedInterfaces();

            builder.AddRabbitMq();
        }
    }
}
