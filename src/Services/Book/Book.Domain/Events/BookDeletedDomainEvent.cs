﻿using eBook.Base.Domain.Events;

namespace Book.Domain.Events
{
    public class BookDeletedDomainEvent : DomainEventBase
    {
        public BookDeletedDomainEvent(int bookId) : base(bookId)
        {
        }
    }
}
