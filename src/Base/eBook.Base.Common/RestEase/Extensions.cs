using System;
using System.Linq;
using System.Net.Http;
using eBook.Base.Common.Consul;
using eBook.Base.Common.Extentions;
using eBook.Base.Common.Fabio;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RestEase;

namespace eBook.Base.Common.RestEase
{
    public static class Extensions
    {
        public static void RegisterServiceForwarder<T>(this IServiceCollection services, string serviceName)
            where T : class
        {
            var clientName = typeof(T).ToString();
            var options = ConfigureOptions(services);

            var service = options.Services.SingleOrDefault(s => s.Name.Equals(serviceName, StringComparison.InvariantCultureIgnoreCase));
            if (service == null)
            {
                throw new RestEaseServiceNotFoundException($"RestEase service: '{serviceName}' was not found.", serviceName);
            }

            switch (options.LoadBalancer?.ToLowerInvariant())
            {
                case "consul":
                    ConfigureConsulClient(services, clientName, service);
                    break;
                case "fabio":
                    ConfigureFabioClient(services, clientName, service);
                    break;
                default:
                    ConfigureDefaultClient(services, clientName, service);
                    break;
            }

            ConfigureForwarder<T>(services, clientName);
        }

        private static RestEaseOptions ConfigureOptions(IServiceCollection services)
        {
            IConfiguration configuration;
            using (var serviceProvider = services.BuildServiceProvider())
            {
                configuration = serviceProvider.GetService<IConfiguration>();
            }

            services.Configure<RestEaseOptions>(configuration.GetSection("restEase"));

            return configuration.GetOptions<RestEaseOptions>("restEase");
        }

        private static void ConfigureConsulClient(IServiceCollection services, string clientName, RestEaseOptions.Service service)
        {
            services.AddHttpClient(clientName)
                .AddHttpMessageHandler(c =>
                    new ConsulServiceDiscoveryMessageHandler(c.GetService<IConsulServicesRegistry>(),
                        c.GetService<IOptions<ConsulOptions>>(), service.Name, service.Version, overrideRequestUri: true));
        }

        private static void ConfigureFabioClient(IServiceCollection services, string clientName, RestEaseOptions.Service service)
        {
            services.AddHttpClient(clientName)
                .AddHttpMessageHandler(c =>
                    new FabioMessageHandler(c.GetService<IOptions<FabioOptions>>(), service.Name, service.Version));
        }

        private static void ConfigureDefaultClient(IServiceCollection services, string clientName, RestEaseOptions.Service service)
        {
            services.AddHttpClient(clientName, client =>
            {
                client.BaseAddress = new UriBuilder
                {
                    Scheme = service.Scheme,
                    Host = service.Host,
                    Port = service.Port
                }.Uri;
            });
        }

        private static void ConfigureForwarder<T>(IServiceCollection services, string clientName) where T : class
        {
            services.AddTransient(c => new RestClient(c.GetService<IHttpClientFactory>().CreateClient(clientName))
            {
                RequestQueryParamSerializer = new QueryParamSerializer()
            }.For<T>());
        }
    }
}