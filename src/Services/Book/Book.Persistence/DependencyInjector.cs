﻿using Book.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Book.Persistence
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<BookDbContext>(options =>
            {
                //options.UseSqlServer(configuration.GetConnectionString("SqlServer"));
                options.UseSqlite(configuration.GetConnectionString("Sqlite"));
            });

            services.AddScoped<IBookDbContext>(provider => provider.GetService<BookDbContext>());

            services.AddScoped<IBookRepository, BookRepository>();

            return services;
        }
    }
}
