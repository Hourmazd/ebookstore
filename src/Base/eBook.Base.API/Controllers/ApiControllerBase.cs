﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using eBook.Base.Common.Types;
using OpenTracing;
using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Mvc;
using System.Reflection;

namespace eBook.Base.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class ApiControllerBase : ControllerBase
    {
        protected readonly string AcceptLanguageHeader = "accept-language";
        protected readonly string OperationHeader = "X-Operation";
        protected readonly string ResourceHeader = "X-Resource";
        protected readonly string DefaultCulture = "en-us";

        protected ITracer Tracer { get; }
        protected IServiceId ServiceId { get; }

        protected ApiControllerBase(ITracer tracer, IServiceId serviceId)
        {
            Tracer = tracer;
            ServiceId = serviceId;
        }

        protected ICorrelationContext GetContext<T>(Guid? resourceId = null, string resource = "")
            where T : ICommand
        {
            if (!string.IsNullOrWhiteSpace(resource))
            {
                resource = $"{resource}/{resourceId}";
            }

            var resourceName = !string.IsNullOrWhiteSpace(resource) ? resource : Assembly.GetCallingAssembly().FullName;
            return CorrelationContext.Create<T>(Guid.NewGuid(), UserId, resourceId ?? new Guid(ServiceId.Id),
               HttpContext.TraceIdentifier, HttpContext.Connection.Id, Tracer.ActiveSpan.Context.ToString(),
               Request.Path.ToString(), Culture, resourceName);
        }

        protected bool IsAdmin
            => User.IsInRole("admin");

        protected Guid UserId
            => string.IsNullOrWhiteSpace(User?.Identity?.Name) ?
                Guid.Empty :
                Guid.Parse(User.Identity.Name);

        protected string Culture
            => Request.Headers.ContainsKey(AcceptLanguageHeader) ?
                    Request.Headers[AcceptLanguageHeader].First().ToLowerInvariant() :
                    DefaultCulture;
    }
}
