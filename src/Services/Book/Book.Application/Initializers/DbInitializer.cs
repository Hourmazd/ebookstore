﻿using eBook.Base.Common.Types;
using MediatR;
using System.Threading.Tasks;

namespace Book.Application.Initializers
{
    public class DbInitializer : ISqlDbInitializer
    {
        private readonly IMediator _Mediator;

        public DbInitializer(IMediator mediator)
        {
            _Mediator = mediator;
        }

        public async Task InitializeAsync()
        {
            var seeder = new SampleDataSeeder(_Mediator);
            await seeder.SeedData();
        }
    }
}
