﻿using Warehouse.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Warehouse.Persistence.EntityConfiguration
{
    public class WarehouseEntityTypeConfiguration : IEntityTypeConfiguration<WarehouseEntity>
    {
        public void Configure(EntityTypeBuilder<WarehouseEntity> builder)
        {
            builder.ToTable("Warehouse", WarehouseDbContext.DEFAULT_SCHEMA);

            builder.Property(p => p.Price)
                .HasColumnType("decimal(10,2)")
                .IsRequired();
        }
    }
}
