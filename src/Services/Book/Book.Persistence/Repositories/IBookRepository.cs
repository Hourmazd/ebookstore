﻿using Book.Domain.Entities;
using eBook.Base.Persistence.Repositories;

namespace Book.Persistence.Repositories
{
    public interface IBookRepository : IRepositoryBase<BookEntity>
    {
    }
}
