﻿using AutoMapper;
using Warehouse.Application.ViewModels;
using Warehouse.Domain.Entities;
using Warehouse.Persistence.Repositories;
using eBook.Base.Application.Queries;
using eBook.Base.Common.RabbitMq;

namespace Warehouse.Application.Queries
{
    public class WarehouseListQuery : ListQueryBase<WarehouseEntity, WarehouseDTO>
    {
        public WarehouseListQuery(ICorrelationContext context = null)
            : base(new WarehouseDataFetchSpecification(), context)
        {
        }
    }

    public class WarehouseListQueryHandler
        : ListQueryHandlerBase<WarehouseEntity, WarehouseDTO, IWarehouseRepository, WarehouseListQuery>
    {
        public WarehouseListQueryHandler(IWarehouseRepository repository, IMapper mapper)
            : base(repository, mapper)
        {
        }
    }
}
