﻿using AutoMapper;
using Warehouse.Domain.Entities;
using eBook.Base.Application.ViewModels;

namespace Warehouse.Application.ViewModels
{
    [AutoMap(typeof(WarehouseEntity))]

    public class WarehouseDTO : DTOBase
    {
        public int BookId { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public int BookedQuantity { get; set; }
    }
}
