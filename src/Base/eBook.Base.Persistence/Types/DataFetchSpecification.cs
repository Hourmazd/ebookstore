﻿using eBook.Base.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace eBook.Base.Persistence.Types
{
    public abstract class DataFetchSpecification<TEntity> : IDataFetchSpecification<TEntity>
        where TEntity : EntityBase
    {
        protected DataFetchSpecification()
        {
            Sorts.Add(e => e.Id);
        }

        public Expression<Func<TEntity, bool>> Criteria { get; set; }

        protected IList<Expression<Func<TEntity, object>>> Includes { get; } = new List<Expression<Func<TEntity, object>>>();

        protected IList<Expression<Func<TEntity, object>>> Sorts { get; } = new List<Expression<Func<TEntity, object>>>();

        public Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> SortExpression { get; set; }

        public Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> IncludeExpression { get; set; }

        public bool IsPagingEnabled { get; set; } = false;

        public int Page { get; set; }

        public int PageCount { get; set; }

        public IQueryable<TEntity> Apply(IQueryable<TEntity> query)
        {
            var result = query;

            if (Criteria != null)
                result = result.Where(Criteria);

            if (IncludeExpression != null)
            {
                result = IncludeExpression(query);
            }
            else
            {
                if (Includes.Any())
                    result = Includes.Aggregate(result, (current, include) => current.Include(include));
            }

            if (SortExpression != null)
            {
                result = SortExpression(result);
            }
            else
            {
                if (Sorts.Any())
                    result = Sorts.Aggregate(result, (current, sort) => current.OrderBy(sort));
            }

            if (IsPagingEnabled)
            {
                result = result
                    .Skip((Page - 1) * PageCount)
                    .Take(PageCount);
            }

            return result;
        }
    }
}
