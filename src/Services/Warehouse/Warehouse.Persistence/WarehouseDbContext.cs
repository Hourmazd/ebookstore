﻿using Warehouse.Domain.Entities;
using Warehouse.Persistence.EntityConfiguration;
using eBook.Base.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Warehouse.Persistence
{
    public class WarehouseDbContext : DbContextBase, IWarehouseDbContext
    {
        #region Constructors

        public WarehouseDbContext(DbContextOptions<WarehouseDbContext> options)
            : base(options)
        {
            System.Diagnostics.Debug.WriteLine($"WarehouseDbContext::ctor {GetHashCode()}");
        }

        #endregion

        #region Public Properties

        public const string DEFAULT_SCHEMA = "dbo";

        #endregion

        #region DbSets

        public DbSet<WarehouseEntity> Warehouse { get; set; }

        #endregion

        #region Override Members

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new WarehouseEntityTypeConfiguration());
        }

        #endregion
    }
}
