namespace eBook.Base.Common.Mvc
{
    public interface IServiceId
    {
         string Id { get; }
    }
}