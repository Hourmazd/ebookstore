﻿using Book.Domain.Entities;
using eBook.Base.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Book.Persistence
{
    public interface IBookDbContext : IDbContextBase
    {
        DbSet<BookEntity> Books { get; set; }
    }
}
