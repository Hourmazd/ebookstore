﻿
namespace eBook.Base.Common.IntegrationEvents
{
    public interface IRejectedIntegrationEvent : IIntegrationEvent
    {
        string Reason { get; }
        string Code { get; }
    }
}
