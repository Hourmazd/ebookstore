using eBook.Base.Common.IntegrationEvents;
using eBook.Base.Common.Types;
using System.Threading.Tasks;

namespace eBook.Base.Common.RabbitMq
{
    public interface IBusPublisher
    {
        Task SendAsync<TCommand>(TCommand command, ICorrelationContext context)
            where TCommand : ICommand;

        Task PublishAsync<TEvent>(TEvent @event, ICorrelationContext context)
            where TEvent : IIntegrationEvent;
    }
}