﻿using eBook.Base.Common.Extentions;
using System;
using App.Metrics;
using Microsoft.AspNetCore.Hosting;
using App.Metrics.AspNetCore;

namespace eBook.Base.Common.Metrics
{
    public static class Extensions
    {
        private static bool _initialized;

        public static IWebHostBuilder UseDefaultMetrics(this IWebHostBuilder webHostBuilder)
        {
            if (_initialized)
            {
                return webHostBuilder;
            }

            return webHostBuilder
                .ConfigureMetricsWithDefaults((context, builder) =>
                {
                    var metricsOptions = context.Configuration.GetOptions<MetricsOptions>("metrics");

                    if (!metricsOptions.Enabled)
                    {
                        return;
                    }

                    _initialized = true;
                    builder.Configuration.Configure(cfg =>
                    {
                        var tags = metricsOptions.Tags;
                        if (tags is null)
                        {
                            return;
                        }

                        tags.TryGetValue("app", out var app);
                        tags.TryGetValue("env", out var env);
                        tags.TryGetValue("server", out var server);
                        cfg.AddAppTag(string.IsNullOrWhiteSpace(app) ? null : app);
                        cfg.AddEnvTag(string.IsNullOrWhiteSpace(env) ? null : env);
                        cfg.AddServerTag(string.IsNullOrWhiteSpace(server) ? null : server);
                        foreach (var tag in tags)
                        {
                            if (!cfg.GlobalTags.ContainsKey(tag.Key))
                            {
                                cfg.GlobalTags.Add(tag.Key, tag.Value);
                            }
                        }
                    }
                    );

                    if (metricsOptions.InfluxEnabled)
                    {
                        builder.Report.ToInfluxDb(o =>
                        {
                            o.InfluxDb.Database = metricsOptions.Database;
                            o.InfluxDb.BaseUri = new Uri(metricsOptions.InfluxUrl);
                            o.InfluxDb.CreateDataBaseIfNotExists = true;
                            o.FlushInterval = TimeSpan.FromSeconds(metricsOptions.Interval);
                        });
                    }
                })
                //.UseHealth()
                //.UseHealthEndpoints()
                .UseMetricsWebTracking()
                .UseMetrics();
        }
    }
}
