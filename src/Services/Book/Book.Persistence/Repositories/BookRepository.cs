﻿using Book.Domain.Entities;
using eBook.Base.Persistence.Repositories;
using MediatR;

namespace Book.Persistence.Repositories
{
    public class BookRepository : RepositoryBase<BookDbContext, BookEntity>, IBookRepository
    {
        public BookRepository(BookDbContext context, IMediator mediator)
            : base(context, mediator)
        {
        }
    }
}
