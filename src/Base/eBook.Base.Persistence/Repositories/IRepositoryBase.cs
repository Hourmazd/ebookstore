﻿using eBook.Base.Domain.Entities;
using eBook.Base.Persistence.Types;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace eBook.Base.Persistence.Repositories
{
    public interface IRepositoryBase<TEntity>
        where TEntity : EntityBase
    {
        IQueryable<TEntity> QueryableList { get; }

        Task<IEnumerable<TEntity>> GetList(IDataFetchSpecification<TEntity> specification = null, CancellationToken cancellationToken = default);

        Task<TEntity> GetEntityById(int id, CancellationToken cancellationToken = default);

        Task<TEntity> Add(TEntity entity, CancellationToken cancellationToken = default);

        Task<bool> Remove(TEntity entity, CancellationToken cancellationToken = default);

        Task<bool> Update(TEntity entity, CancellationToken cancellationToken = default);
    }
}
