﻿using Warehouse.Application.Commands;
using Warehouse.Application.Queries;
using Warehouse.Application.ViewModels;
using Warehouse.Domain.Entities;
using eBook.Base.API.Controllers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using OpenTracing;
using System.Collections.Generic;
using eBook.Base.Common.Mvc;
using System.Threading.Tasks;
using Warehouse.Application.Forwarders;

namespace Warehouse.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WarehouseController : CqrsApiControllerBase<WarehouseEntity, WarehouseDTO>
    {
        private IBookForwarder _BookForwarder;
        public WarehouseController(IMediator mediator, ITracer tracer, IServiceId serviceId, IBookForwarder bookForwarder)
            : base(mediator, tracer, serviceId)
        {
            _BookForwarder = bookForwarder;
        }

        protected override IRequest UpsertCommand(WarehouseDTO dto)
        {
            return new WarehouseUpsertCommand()
            {
                Id = dto.Id,
                BookId = dto.BookId,
                Price = dto.Price,
                Quantity = dto.Quantity
            };
        }

        protected override IRequest DeleteCommand(int entityId)
        {
            return new WarehouseDeleteCommand() { Id = entityId };
        }

        protected override IRequest<IEnumerable<WarehouseDTO>> FetchListCommand()
        {
            return new WarehouseListQuery();
        }

        protected override IRequest<WarehouseDTO> FetchSingleCommand(int entityId)
        {
            return new WarehouseSingleQuery() { EntityId = entityId };
        }
    }
}