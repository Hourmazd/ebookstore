﻿using eBook.Base.Application.ViewModels;

namespace Warehouse.Application.ViewModels
{
    public class BookDTO : DTOBase
    {
        public string ISBN { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public decimal Price { get; set; }
    }
}
