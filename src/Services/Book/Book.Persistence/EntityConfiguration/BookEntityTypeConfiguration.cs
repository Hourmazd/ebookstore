﻿using Book.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Book.Persistence.EntityConfiguration
{
    public class BookEntityTypeConfiguration : IEntityTypeConfiguration<BookEntity>
    {
        public void Configure(EntityTypeBuilder<BookEntity> builder)
        {
            builder.ToTable("Books", BookDbContext.DEFAULT_SCHEMA);

            builder.Property(p => p.Price)
                .HasColumnType("decimal(10,2)")
                .IsRequired();
        }
    }
}
