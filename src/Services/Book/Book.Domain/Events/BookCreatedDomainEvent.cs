﻿using Book.Domain.Entities;
using eBook.Base.Domain.Events;

namespace Book.Domain.Events
{
    public class BookCreatedDomainEvent : DomainEventBase<BookEntity>
    {
        public BookCreatedDomainEvent(BookEntity book) : base(book)
        {
        }
    }
}
