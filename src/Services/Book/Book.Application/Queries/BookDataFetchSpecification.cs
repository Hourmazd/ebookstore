﻿using Book.Domain.Entities;
using eBook.Base.Persistence.Types;

namespace Book.Application.Queries
{
    public class BookDataFetchSpecification : DataFetchSpecification<BookEntity>
    {
    }
}
