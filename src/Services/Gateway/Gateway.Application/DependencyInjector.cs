﻿using AutoMapper;
using eBook.Base.Common.RestEase;
using Gateway.Application.Forwarders;
using Gateway.Application.Services;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Gateway.Application
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddMediatR(Assembly.GetExecutingAssembly())
                .AddAutoMapper(Assembly.GetExecutingAssembly())
                .AddCustomHealthCheck(configuration);

            services.RegisterServiceForwarder<IBookForwarder>("book-service");
            services.RegisterServiceForwarder<IWarehouseForwarder>("warehouse-service");

            services.AddTransient<BookService>();

            return services;
        }

        public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            return services;
        }
    }
}
