﻿using Warehouse.Domain.Entities;
using eBook.Base.Persistence.Repositories;

namespace Warehouse.Persistence.Repositories
{
    public interface IWarehouseRepository : IRepositoryBase<WarehouseEntity>
    {
    }
}
