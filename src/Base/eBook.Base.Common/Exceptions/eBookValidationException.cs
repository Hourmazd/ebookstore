﻿namespace eBook.Base.Common.Exceptions
{
    public class eBookValidationException : eBookException
    {
        public eBookValidationException(string message)
            : base("validation_error", message)
        {
        }
    }
}
