﻿
using eBook.Base.Common.Mvc;

namespace eBook.Base.Common
{
    public class AppOptions
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string Scheme { get; set; }
        public string Address { get; set; }
        public int Port { get; set; }
        public string FullAddress => Scheme + (Scheme.EndsWith("://") ? string.Empty : "://") + Address;
        public string FullAddressWithPort => FullAddress + ":" + Port;
    }
}
