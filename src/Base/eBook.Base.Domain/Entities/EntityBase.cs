﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MediatR;

namespace eBook.Base.Domain.Entities
{
    public abstract class EntityBase
    {
        #region Private Fields

        private int? _RequestedHashCode;
        private readonly List<INotification> _DomainEvents = new List<INotification>();

        #endregion

        #region Public Properties

        [Key]
        public virtual int Id { get; protected set; }

        //[Timestamp]
        //public byte[] Timestamp { get; set; }

        [NotMapped]
        public virtual bool IsTransient => Id == default;

        [NotMapped]
        public virtual bool IsDirty { get; protected set; }

        [NotMapped]
        public IReadOnlyCollection<INotification> DomainEvents => _DomainEvents.AsReadOnly();

        #endregion

        #region Public Methods

        public void SetDirty()
        {
            IsDirty = true;
        }

        public void AddDomainEvent(INotification eventItem)
        {
            _DomainEvents?.Add(eventItem);
        }

        public void RemoveDomainEvent(INotification eventItem)
        {
            _DomainEvents?.Remove(eventItem);
        }

        public void ClearDomainEvents()
        {
            _DomainEvents?.Clear();
        }

        #endregion

        #region Override Members

        public override bool Equals(object obj)
        {
            if (obj is null || !(obj is EntityBase))
                return false;

            if (ReferenceEquals(this, obj))
                return true;

            if (GetType() != obj.GetType())
                return false;

            var other = (EntityBase)obj;

            if (other.IsTransient || IsTransient)
                return false;
            else
                return other.Id == Id;
        }

        public override int GetHashCode()
        {
            if (!IsTransient)
            {
                if (!_RequestedHashCode.HasValue)
                    _RequestedHashCode = Id.GetHashCode() ^ 31; // XOR for random distribution (http://blogs.msdn.com/b/ericlippert/archive/2011/02/28/guidelines-and-rules-for-gethashcode.aspx)

                return _RequestedHashCode.Value;
            }

            return base.GetHashCode();
        }

        #endregion

        #region Operator Extentions

        public static bool operator ==(EntityBase left, EntityBase right)
        {
            if (Equals(left, null))
                return Equals(right, null);
            else
                return left.Equals(right);
        }

        public static bool operator !=(EntityBase left, EntityBase right)
        {
            return !(left == right);
        }

        #endregion
    }
}
