using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using eBook.Base.API.Controllers;
using eBook.Base.Common.Mvc;
using eBook.Base.Common;

namespace Book.API.Controllers
{
    [Route("")]
    [ApiController]
    public class HomeController : HomeControllerBase
    {
        public HomeController(IOptions<AppOptions> options, IServiceId serviceId) 
            : base(options, serviceId)
        {
        }
    }
}