using RawRabbit.Configuration;

namespace eBook.Base.Common.RabbitMq
{
    public class RabbitMqOptions : RawRabbitConfiguration
    {
        public bool Enabled { get; set; }
        public string Namespace { get; set; }
        public int Retries { get; set; }
        public int RetryInterval { get; set; }
    }
}