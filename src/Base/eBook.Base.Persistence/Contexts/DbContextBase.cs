﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace eBook.Base.Persistence.Contexts
{
    public abstract class DbContextBase : DbContext, IDbContextBase
    {
        #region Constructors

        protected DbContextBase(DbContextOptions options)
            : base(options)
        {
        }

        #endregion

        #region Private Fields

        private IDbContextTransaction _CurrentTransaction;

        #endregion

        #region Public Methods

        public async Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            if (_CurrentTransaction != null)
                return null;

            _CurrentTransaction = await Database.BeginTransactionAsync(cancellationToken);

            return _CurrentTransaction;
        }

        public async Task CommitTransactionAsync(IDbContextTransaction transaction)
        {
            if (transaction is null)
                throw new ArgumentNullException(nameof(transaction));

            if (transaction != _CurrentTransaction)
                throw new InvalidOperationException($"Transaction {transaction.TransactionId} is not current.");

            try
            {
                await SaveChangesAsync();
                transaction.Commit();
            }
            catch
            {
                await RollbackTransactionAsync();
                throw;
            }
            finally
            {
                if (_CurrentTransaction != null)
                {
                    await _CurrentTransaction.DisposeAsync();
                    _CurrentTransaction = null;
                }
            }
        }

        public async Task RollbackTransactionAsync(CancellationToken cancellationToken = default)
        {
            if (_CurrentTransaction is null)
                return;

            try
            {
                await _CurrentTransaction.RollbackAsync(cancellationToken);
            }
            finally
            {
                if (_CurrentTransaction != null)
                {
                    await _CurrentTransaction.DisposeAsync();
                    _CurrentTransaction = null;
                }
            }
        }

        #endregion
    }
}
