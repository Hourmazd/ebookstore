using System.Threading.Tasks;

namespace eBook.Base.Common.Types
{
    public interface IInitializer
    {
        Task InitializeAsync();
    }
}