﻿using AutoMapper;
using eBook.Base.Application.ViewModels;
using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Types;
using eBook.Base.Domain.Entities;
using eBook.Base.Persistence.Repositories;
using eBook.Base.Persistence.Types;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace eBook.Base.Application.Queries
{
    public class ListQueryBase<TEntity, TDTO> : IRequest<IEnumerable<TDTO>>, ICommand
        where TEntity : EntityBase
        where TDTO : DTOBase
    {
        public ICorrelationContext Context { get; set; }
               
        public IDataFetchSpecification<TEntity> Specification { get; }

        public ListQueryBase(IDataFetchSpecification<TEntity> specification, ICorrelationContext context)
        {
            Specification = specification;
            Context = context;
        }

        public void SetContext(ICorrelationContext context)
        {
            Context = context;
        }
    }

    public class ListQueryHandlerBase<TEntity, TDTO, TRepository, TCommand> : IRequestHandler<TCommand, IEnumerable<TDTO>>
        where TEntity : EntityBase
        where TDTO : DTOBase
        where TCommand : ListQueryBase<TEntity, TDTO>
        where TRepository : IRepositoryBase<TEntity>
    {
        protected readonly TRepository Repository;
        protected readonly IMapper Mapper;

        public ListQueryHandlerBase(TRepository repository, IMapper mapper)
        {
            Repository = repository;
            Mapper = mapper;
        }

        public async Task<IEnumerable<TDTO>> Handle(TCommand request, CancellationToken cancellationToken)
        {
            var result = await Repository.GetList(request.Specification, cancellationToken);

            return Mapper.Map<IEnumerable<TDTO>>(result);
        }
    }
}
