﻿using System.Threading.Tasks;
using eBook.Base.API.Controllers;
using eBook.Base.Common.Mvc;
using Gateway.Application.Services;
using Gateway.Application.ViewModels;
using Microsoft.AspNetCore.Mvc;
using OpenTracing;

namespace Gateway.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ApiControllerBase
    {
        private readonly BookService BookService;

        public BookController(ITracer tracer, IServiceId serviceId, BookService bookService)
            :base(tracer, serviceId)
        {
            BookService = bookService;
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<BookDTO>> Get(int id)
        {
            return Ok(await BookService.GetBook(id));
        }

        [HttpPost("{id:int}")]
        public async Task<ActionResult> Upsert(int id)
        {
            await BookService.Upsert(id);
            return Ok();
        }
    }
}