﻿using AutoMapper;
using Book.Application.ViewModels;
using Book.Domain.Entities;
using Book.Persistence.Repositories;
using eBook.Base.Application.Queries;
using eBook.Base.Common.RabbitMq;

namespace Book.Application.Queries
{
    public class BookListQuery : ListQueryBase<BookEntity, BookDTO>
    {
        public BookListQuery(ICorrelationContext context = null)
            : base(new BookDataFetchSpecification(), context)
        {
        }
    }

    public class BookListQueryHandler
        : ListQueryHandlerBase<BookEntity, BookDTO, IBookRepository, BookListQuery>
    {
        public BookListQueryHandler(IBookRepository repository, IMapper mapper)
            : base(repository, mapper)
        {
        }
    }
}
