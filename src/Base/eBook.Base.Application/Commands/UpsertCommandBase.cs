﻿using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Types;
using eBook.Base.Domain.Entities;
using eBook.Base.Persistence.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace eBook.Base.Application.Commands
{
    public abstract class UpsertCommandBase : IRequest, ICommand
    {
        public ICorrelationContext Context { get; private set; }

        public int Id { get; set; }

        protected UpsertCommandBase(ICorrelationContext context)
        {
            Context = context;
        }

        public void SetContext(ICorrelationContext context)
        {
            Context = context;
        }
    }

    public abstract class UpsertCommandHandlerBase<TEntity, TCommand, TRepository> : IRequestHandler<TCommand>
        where TEntity : EntityBase
        where TCommand : UpsertCommandBase
        where TRepository : IRepositoryBase<TEntity>
    {
        protected readonly TRepository _Repository;
        protected readonly IBusPublisher _BusPublisher;

        public UpsertCommandHandlerBase(TRepository repository, IBusPublisher busPublisher)
        {
            _Repository = repository;
            _BusPublisher = busPublisher;
        }

        public async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
        {
            var entity = await _Repository.GetEntityById(request.Id, cancellationToken);

            entity = await Complete(request, entity, cancellationToken);

            if (entity.IsTransient)
                await _Repository.Add(entity, cancellationToken);
            else if (entity.IsDirty)
                await _Repository.Update(entity, cancellationToken);

            return Unit.Value;
        }

        protected abstract Task<TEntity> Complete(TCommand command, TEntity entity, CancellationToken cancellationToken);
    }
}
