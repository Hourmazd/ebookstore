﻿using Warehouse.Application.Commands;
using MediatR;
using Warehouse.Application.Queries;
using System.Threading.Tasks;

namespace Warehouse.Application.Initializers
{
    public class SampleDataSeeder
    {
        private readonly IMediator _Mediator;

        public SampleDataSeeder(IMediator mediator)
        {
            _Mediator = mediator;
        }

        public async Task SeedData()
        {
            for (int i = 1; i <= 10; i++)
            {
                var query = new WarehouseSingleQuery() { EntityId = i };
                var entity = await _Mediator.Send(query);

                if (entity is null)
                {
                    var command = new WarehouseUpsertCommand()
                    {
                        Id = i,
                        BookId = i,
                        Quantity = i
                    };

                   await _Mediator.Send(command);
                }
            }
        }
    }
}
