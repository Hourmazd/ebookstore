﻿using Book.Domain.Events;
using Book.Persistence.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Book.Application.EventHandlers
{
    public class BookDeletedDomainEventHandler : INotificationHandler<BookDeletedDomainEvent>
    {
        private readonly ILogger<BookDeletedDomainEventHandler> _Logger;
        private readonly IBookRepository _Repository;

        public BookDeletedDomainEventHandler(
            ILogger<BookDeletedDomainEventHandler> logger,
            IBookRepository repository)
        {
            _Logger = logger;
            _Repository = repository;
        }

        public Task Handle(BookDeletedDomainEvent notification, CancellationToken cancellationToken)
        {
            _Logger.LogInformation($"\t>> Book {notification.EntityId} has been deleted.");

            return Task.CompletedTask;
        }
    }
}
