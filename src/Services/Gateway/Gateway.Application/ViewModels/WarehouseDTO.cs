﻿using eBook.Base.Application.ViewModels;

namespace Gateway.Application.ViewModels
{
    public class WarehouseDTO : DTOBase
    {
        public int BookId { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public int BookedQuantity { get; set; }
    }
}
