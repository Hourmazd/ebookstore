﻿using Book.Domain.Events;
using Book.Persistence.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Book.Domain.EventHandlers
{
    public class BookCreatedDomainEventHandler : INotificationHandler<BookCreatedDomainEvent>
    {
        private readonly ILogger<BookCreatedDomainEventHandler> _Logger;
        private readonly IBookRepository _Repository;

        public BookCreatedDomainEventHandler(
            ILogger<BookCreatedDomainEventHandler> logger,
            IBookRepository repository)
        {
            _Logger = logger;
            _Repository = repository;
        }

        public Task Handle(BookCreatedDomainEvent notification, CancellationToken cancellationToken)
        {
            _Logger.LogInformation($"\t>> Book {notification.EntityId} has been created.");

            return Task.CompletedTask;
        }
    }
}
