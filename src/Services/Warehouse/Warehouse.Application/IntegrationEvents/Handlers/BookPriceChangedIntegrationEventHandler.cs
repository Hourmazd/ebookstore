﻿using Warehouse.Application.Forwarders;
using Warehouse.Application.IntegrationEvents.Events;
using eBook.Base.Common.Handlers;
using eBook.Base.Common.RabbitMq;
using System.Threading.Tasks;
using MediatR;
using Warehouse.Application.Commands;
using Warehouse.Application.Queries;
using System.Linq;
using eBook.Base.Common.Exceptions;
using System;

namespace Warehouse.Application.IntegrationEvents.Handlers
{
    public class BookPriceChangedIntegrationEventHandler : IEventHandler<BookPriceChangedIntegrationEvent>
    {
        private readonly IBookForwarder _BookForwarder;
        private readonly IMediator _Mediator;

        public BookPriceChangedIntegrationEventHandler(
            IBookForwarder bookForwarder,
            IMediator mediator)
        {
            _BookForwarder = bookForwarder;
            _Mediator = mediator;
        }

        public async Task HandleAsync(BookPriceChangedIntegrationEvent @event, ICorrelationContext context)
        {
            var book = await _BookForwarder.GetBook(@event.BookId);

            if (book is null)
            {
                throw new eBookException($"Unable to fetch book id: {@event.BookId}");
            }
            else
            {
                var query = new WarehouseListQuery(context);
                query.Specification.Criteria = e => e.BookId == @event.BookId;
                var warehouseItems = await _Mediator.Send(query);

                if (warehouseItems.Any())
                    foreach (var item in warehouseItems)
                    {
                        var command = new WarehouseUpsertCommand(context)
                        {
                            Id = item.Id,
                            BookId = item.BookId,
                            Price = @event.NewPrice
                        };
                        await _Mediator.Send(command);
                    }
            }
        }
    }
}
