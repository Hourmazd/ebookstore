﻿using System;
using eBook.Base.Common.IntegrationEvents;
using eBook.Base.Common.Exceptions;
using eBook.Base.Common.Types;

namespace eBook.Base.Common.RabbitMq
{
    public interface IBusSubscriber
    {
        IBusSubscriber SubscribeCommand<TCommand>(string @namespace = null, string queueName = null,
            Func<TCommand, eBookException, IRejectedIntegrationEvent> onError = null)
            where TCommand : ICommand;

        IBusSubscriber SubscribeEvent<TEvent>(string @namespace = null, string queueName = null,
            Func<TEvent, eBookException, IRejectedIntegrationEvent> onError = null) 
            where TEvent : IIntegrationEvent;
    }
}
