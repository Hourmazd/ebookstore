﻿namespace eBook.Base.Common.Consul
{
    public static class ConsulHelper
    {
        public static string GetConsulServiceName(AppOptions appOptions)
            => GetConsulServiceName(appOptions.Name, appOptions.Version);

        public static string GetConsulServiceName(string serviceName, string serviceVersion)
            => serviceName + (string.IsNullOrWhiteSpace(serviceVersion) ? string.Empty : $"[v{serviceVersion.Replace(".", "_")}]");

    }
}
