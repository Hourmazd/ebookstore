﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace eBook.Base.Persistence.Contexts
{
    public interface IDbContextBase : IDisposable
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        Task<IDbContextTransaction> BeginTransactionAsync(CancellationToken cancellationToken = default);

        Task CommitTransactionAsync(IDbContextTransaction transaction);

        Task RollbackTransactionAsync(CancellationToken cancellationToken = default);
    }
}
