﻿using eBook.Base.Domain.Entities;
using System.ComponentModel.DataAnnotations;

namespace Warehouse.Domain.Entities
{
    public class WarehouseEntity : EntityBase
    {
        #region Constructors

        public WarehouseEntity() : base()
        {
        }

        #endregion

        #region Private Fields
        #endregion

        #region Properties

        [Required]
        public int BookId { get; protected set; }

        [Required]
        public decimal Price { get; protected set; }

        [Required]
        public int Quantity { get; protected set; }

        [Required]
        public int BookedQuantity { get; protected set; }

        #endregion

        #region Public Methods
        #endregion
    }
}
