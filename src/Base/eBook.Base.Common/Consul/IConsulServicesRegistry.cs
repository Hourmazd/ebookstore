using System.Threading.Tasks;
using Consul;

namespace eBook.Base.Common.Consul
{
    public interface IConsulServicesRegistry
    {
        Task<AgentService> GetAsync(string name);
    }
}