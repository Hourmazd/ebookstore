﻿using AutoMapper;
using Warehouse.Application.ViewModels;
using Warehouse.Domain.Entities;
using Warehouse.Persistence.Repositories;
using eBook.Base.Application.Queries;
using eBook.Base.Common.RabbitMq;

namespace Warehouse.Application.Queries
{
    public class WarehouseSingleQuery : SingleQueryBase<WarehouseDTO>
    {
        public WarehouseSingleQuery(ICorrelationContext context = null)
            : base(context) { }
    }

    public class WarehouseSingleQueryHandler
        : SingleQueryHandlerBase<WarehouseEntity, WarehouseDTO, IWarehouseRepository, WarehouseSingleQuery>
    {
        public WarehouseSingleQueryHandler(IWarehouseRepository repository, IMapper mapper)
            : base(repository, mapper)
        {
        }
    }
}
