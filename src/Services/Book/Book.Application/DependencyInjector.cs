﻿using AutoMapper;
using Book.Application.Initializers;
using Book.Application.IntegrationEvents.Events;
using Book.Persistence;
using eBook.Base.Common.Handlers;
using eBook.Base.Common.RestEase;
using eBook.Base.Common.Types;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Book.Application
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddMediatR(Assembly.GetExecutingAssembly())
                .AddAutoMapper(Assembly.GetExecutingAssembly())
                .AddCustomHealthCheck(configuration);

            services.AddScoped<ISqlDbInitializer, DbInitializer>();
            
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ProductValidationBehavior<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(LoggingBehavior<,>));

            //services.AddScoped<IntegrationEventHandler>();
            //services.AddScoped<IntegrationEventPublisher>();

            //var eventBusType = configuration.GetValue<string>("EventBus:Type");

            //if (eventBusType == "Azure")
            //{
            //    services.AddSingleton<IEventBus, ProductServiceBus>();
            //}
            //else if (eventBusType == "RabbitMQ")
            //{
            //    //TODO: Implement specific RabbitMQ for this service
            //    services.AddSingleton<IEventBus, RabbitMQServiceBus>();
            //}

            return services;
        }

        //public static IApplicationBuilder AddIntegrationEventHandlers(this IApplicationBuilder app)
        //{
        //    var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

        //    return app;
        //}

        public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            var hcBuilder = services.AddHealthChecks();

            hcBuilder.AddDbContextCheck<BookDbContext>();

            return services;
        }
    }
}
