﻿using Book.Domain.Entities;
using eBook.Base.Domain.Events;

namespace Book.Domain.Events
{
    public class BookUpdatedDomainEvent : DomainEventBase<BookEntity>
    {
        public BookUpdatedDomainEvent(BookEntity book) : base(book)
        {
        }
    }
}
