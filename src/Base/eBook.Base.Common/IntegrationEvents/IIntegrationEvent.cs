﻿using System;

namespace eBook.Base.Common.IntegrationEvents
{
    public interface IIntegrationEvent
    {
        Guid Id { get; }

        DateTime CreationDate { get; }
    }
}
