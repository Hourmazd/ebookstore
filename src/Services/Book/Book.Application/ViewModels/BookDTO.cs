﻿using AutoMapper;
using Book.Domain.Entities;
using eBook.Base.Application.ViewModels;
using MediatR;

namespace Book.Application.ViewModels
{
    [AutoMap(typeof(BookEntity))]

    public class BookDTO : DTOBase
    {
        public string ISBN { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public decimal Price { get; set; }
    }
}
