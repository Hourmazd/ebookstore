﻿using Warehouse.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Warehouse.Persistence
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<WarehouseDbContext>(options =>
            {
                //options.UseSqlServer(configuration.GetConnectionString("SqlServer"));
                options.UseSqlite(configuration.GetConnectionString("Sqlite"));
            });

            services.AddScoped<IWarehouseDbContext>(provider => provider.GetService<WarehouseDbContext>());

            services.AddScoped<IWarehouseRepository, WarehouseRepository>();

            return services;
        }
    }
}
