﻿using Warehouse.Domain.Entities;
using eBook.Base.Domain.Events;

namespace Warehouse.Domain.Events
{
    public class WarehouseUpdatedDomainEvent : DomainEventBase<WarehouseEntity>
    {
        public WarehouseUpdatedDomainEvent(WarehouseEntity entity) : base(entity)
        {
        }
    }
}
