﻿using AutoMapper;
using eBook.Base.Application.ViewModels;
using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Types;
using eBook.Base.Domain.Entities;
using eBook.Base.Persistence.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace eBook.Base.Application.Queries
{
    public class SingleQueryBase<TDTO> : IRequest<TDTO>, ICommand
        where TDTO : DTOBase
    {
        public ICorrelationContext Context { get; set; }

        public int EntityId { get; set; }

        public SingleQueryBase(ICorrelationContext context)
        {
            Context = context;
        }

        public void SetContext(ICorrelationContext context)
        {
            Context = context;
        }
    }

    public class SingleQueryHandlerBase<TEntity, TDTO, TRepository, TCommand> : IRequestHandler<TCommand, TDTO>
        where TEntity : EntityBase
        where TDTO : DTOBase
        where TCommand : SingleQueryBase<TDTO>
        where TRepository : IRepositoryBase<TEntity>
    {
        protected readonly TRepository Repository;
        protected readonly IMapper Mapper;

        public SingleQueryHandlerBase(TRepository repository, IMapper mapper)
        {
            Repository = repository;
            Mapper = mapper;
        }

        public async Task<TDTO> Handle(TCommand request, CancellationToken cancellationToken)
        {
            var result = await Repository.GetEntityById(request.EntityId, cancellationToken);

            return Mapper.Map<TDTO>(result);
        }
    }
}
