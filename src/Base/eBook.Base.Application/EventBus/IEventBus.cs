﻿using eBook.Base.Common.IntegrationEvents;
using System;
using System.Threading.Tasks;

namespace eBook.Base.Application.EventBus
{
    public interface IEventBus : IDisposable
    {
        Task Subscribe<T>()
            where T : IIntegrationEvent;

        Task Publish(IIntegrationEvent @event);
    }
}
