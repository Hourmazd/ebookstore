﻿using eBook.Base.Common.Exceptions;
using Gateway.Application.Forwarders;
using Gateway.Application.ViewModels;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Gateway.Application.Services
{
    public class BookService
    {
        private readonly ILogger<BookService> Logger;
        private readonly IBookForwarder BookForwarder;
        private readonly IWarehouseForwarder WarehouseForwarder;

        public BookService(
            ILogger<BookService> logger,
            IBookForwarder bookForwarder, 
            IWarehouseForwarder warehouseForwarder)
        {
            Logger = logger;
            BookForwarder = bookForwarder;
            WarehouseForwarder = warehouseForwarder;
        }

        public async Task<BookDTO> GetBook(int id)
        {
            var result = await BookForwarder.Get(id);

            if (result != default && !(result is null))
            {
                //get from warehouse
                result.StoreQuantity = 1111;
            }

            return result;
        }

        public async Task Upsert(int id)
        {
            var result = await BookForwarder.Get(id);
            result.Price = new Random().Next(1, 100);
            await BookForwarder.Upsert(result);
        }
    }
}
