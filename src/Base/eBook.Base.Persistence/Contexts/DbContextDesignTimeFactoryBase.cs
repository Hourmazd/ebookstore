﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace eBook.Base.Persistence.Contexts
{
    public abstract class DbContextDesignTimeFactoryBase<TContext> :
        IDesignTimeDbContextFactory<TContext>
        where TContext : DbContext
    {
        protected abstract string ConnectionStringName { get; }
        protected abstract string ProjectName { get; }
        protected abstract string AspNetCoreEnvironment { get; }

        public TContext CreateDbContext(string[] args)
        {
            var basePath = Directory.GetCurrentDirectory() + string.Format("{0}..{0}{1}", Path.DirectorySeparatorChar, ProjectName);

            return CreateContext(basePath);
        }

        protected virtual TContext CreateContext(string basePath)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.Local.json", optional: true)
                .AddJsonFile($"appsettings.{AspNetCoreEnvironment}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();

            var connectionString = config.GetConnectionString(ConnectionStringName);

            if (string.IsNullOrEmpty(connectionString))
                throw new ArgumentException($"Connection string '{ConnectionStringName}' is null or empty.", nameof(connectionString));

            return CreateNewInstance(connectionString);
        }

        protected abstract TContext CreateNewInstance(string connectionString);
    }
}
