﻿using Warehouse.Domain.Entities;
using eBook.Base.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Warehouse.Persistence
{
    public interface IWarehouseDbContext : IDbContextBase
    {
        DbSet<WarehouseEntity> Warehouse { get; set; }
    }
}
