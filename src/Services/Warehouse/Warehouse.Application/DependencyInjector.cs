﻿using AutoMapper;
using Warehouse.Application.Forwarders;
using Warehouse.Application.Initializers;
using Warehouse.Application.IntegrationEvents.Events;
using Warehouse.Application.IntegrationEvents.Handlers;
using Warehouse.Persistence;
using eBook.Base.Common.Handlers;
using eBook.Base.Common.RestEase;
using eBook.Base.Common.Types;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Warehouse.Application
{
    public static class DependencyInjector
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddMediatR(Assembly.GetExecutingAssembly())
                .AddAutoMapper(Assembly.GetExecutingAssembly())
                .AddCustomHealthCheck(configuration);

            services.AddScoped<ISqlDbInitializer, DbInitializer>();
            services.AddScoped<IEventHandler<BookPriceChangedIntegrationEvent>, BookPriceChangedIntegrationEventHandler>();

            services.RegisterServiceForwarder<IBookForwarder>("book-service");

            return services;
        }

        public static IServiceCollection AddCustomHealthCheck(this IServiceCollection services, IConfiguration configuration)
        {
            var hcBuilder = services.AddHealthChecks();

            hcBuilder.AddDbContextCheck<WarehouseDbContext>();

            return services;
        }
    }
}
