using eBook.Base.Common.Extentions;
using eBook.Base.Common.Mvc;
using Jaeger;
using Jaeger.Reporters;
using Jaeger.Samplers;
using Jaeger.Senders;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenTracing;
using OpenTracing.Util;
using RawRabbit.Instantiation;
using System;

namespace eBook.Base.Common.Jeager
{
    public static class Extensions
    {
        private static bool _initialized;

        public static IServiceCollection AddJaeger(this IServiceCollection services)
        {
            if (_initialized)
            {
                return services;
            }

            _initialized = true;

            var options = GetOptions(services);
            var serviceId = options.Item1;
            var appOptions = options.Item2;
            var jeagerOptions = options.Item3;

            services.AddOpenTracing();

            if (!jeagerOptions.Enabled)
            {
                var defaultTracer = eBookDefaultTracer.Create();
                services.AddSingleton(defaultTracer);
                return services;
            }

            services.AddSingleton<ITracer>(sp =>
            {
                var loggerFactory = sp.GetRequiredService<ILoggerFactory>();

                var reporter = new RemoteReporter
                        .Builder()
                    .WithSender(new UdpSender(jeagerOptions.UdpHost, jeagerOptions.UdpPort, jeagerOptions.MaxPacketSize))
                    .WithLoggerFactory(loggerFactory)
                    .Build();

                var sampler = GetSampler(jeagerOptions);
                var version = string.IsNullOrWhiteSpace(appOptions.Version) ? string.Empty : $" [v{appOptions.Version}]";

                var tracer = new Tracer
                        .Builder($"{appOptions.Name}{version}")
                    .WithReporter(reporter)
                    .WithSampler(sampler)
                    .WithTag("service.version", appOptions.Version)
                    .WithTag("service.id", serviceId.Id)
                    .Build();

                GlobalTracer.Register(tracer);

                return tracer;
            });

            return services;
        }

        public static IClientBuilder UseJaeger(this IClientBuilder builder, ITracer tracer)
        {
            builder.Register(pipe => pipe
                .Use<JaegerStagedMiddleware>(tracer));
            return builder;
        }

        private static Tuple<IServiceId, AppOptions, JaegerOptions> GetOptions(IServiceCollection services)
        {
            using (var serviceProvider = services.BuildServiceProvider())
            {
                var configuration = serviceProvider.GetService<IConfiguration>();
                services.Configure<JaegerOptions>(configuration.GetSection("jaeger"));
                var jaegerOptions = configuration.GetOptions<JaegerOptions>("jaeger");
                var appOptions = serviceProvider.GetService<IOptions<AppOptions>>();
                var serviceId = serviceProvider.GetService<IServiceId>();

                return new Tuple<IServiceId, AppOptions, JaegerOptions>(serviceId, appOptions.Value, jaegerOptions);
            }
        }

        private static ISampler GetSampler(JaegerOptions options)
        {
            switch (options.Sampler)
            {
                case "const": return new ConstSampler(true);
                case "rate": return new RateLimitingSampler(options.MaxTracesPerSecond);
                case "probabilistic": return new ProbabilisticSampler(options.SamplingRate);
                default: return new ConstSampler(true);
            }
        }
    }
}