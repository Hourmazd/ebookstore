﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace eBook.Base.Domain.Entities
{
    public static class EntityExtentions
    {
        public static void SetProperty<TSource, TProperty>(
            this TSource entity,
            Expression<Func<TSource, TProperty>> property,
            TProperty value)
            where TSource : EntityBase
        {
            Type type = typeof(TSource);

            var member = property.Body as MemberExpression;
            if (member is null)
                throw new ArgumentException(string.Format("Expression '{0}' refers to a method, not a property.", property.ToString()));

            var propInfo = member.Member as PropertyInfo;
            if (propInfo is null)
                throw new ArgumentException(string.Format("Expression '{0}' refers to a field, not a property.", property.ToString()));

            if (type != propInfo.ReflectedType &&
                !type.IsSubclassOf(propInfo.ReflectedType))
                throw new ArgumentException(string.Format("Expression '{0}' refers to a property that is not from type {1}.", property.ToString(), type));

            var preValue = propInfo.GetValue(entity);

            if (preValue != null && preValue.Equals(value))
                return;

            propInfo.SetValue(entity, value);

            entity.SetDirty();
        }
    }
}
