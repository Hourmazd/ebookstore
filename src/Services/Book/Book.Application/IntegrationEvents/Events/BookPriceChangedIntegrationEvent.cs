﻿using eBook.Base.Common.IntegrationEvents;
using eBook.Base.Common.Types;

namespace Book.Application.IntegrationEvents.Events
{
    [MessageNamespace("bookService")]
    public class BookPriceChangedIntegrationEvent : IntegrationEvent
    {
        public int BookId { get; }

        public decimal NewPrice { get; }

        public decimal OldPrice { get; }

        public BookPriceChangedIntegrationEvent(int bookId, decimal newPrice, decimal oldPrice)
        {
            BookId = bookId;
            NewPrice = newPrice;
            OldPrice = oldPrice;
        }
    }
}
