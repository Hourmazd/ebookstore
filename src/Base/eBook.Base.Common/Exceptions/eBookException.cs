using System;

namespace eBook.Base.Common.Exceptions
{
    public class eBookException : Exception
    {
        public string Code { get; }

        public eBookException()
        {
        }

        public eBookException(string code)
        {
            Code = code;
        }

        public eBookException(string message, params object[] args)
            : this(string.Empty, message, args)
        {
        }

        public eBookException(string code, string message, params object[] args)
            : this(null, code, message, args)
        {
        }

        public eBookException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        public eBookException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }
    }
}