﻿using Warehouse.Domain.Entities;
using Warehouse.Domain.Events;
using Warehouse.Persistence.Repositories;
using eBook.Base.Application.Commands;
using eBook.Base.Common.RabbitMq;
using eBook.Base.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace Warehouse.Application.Commands
{
    public class WarehouseUpsertCommand : UpsertCommandBase
    {
        public WarehouseUpsertCommand(ICorrelationContext context = null)
            : base(context) { }

        public int BookId { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }
    }

    public class WarehouseUpsertCommandHandler
        : UpsertCommandHandlerBase<WarehouseEntity, WarehouseUpsertCommand, IWarehouseRepository>
    {
        public WarehouseUpsertCommandHandler(IWarehouseRepository repository, IBusPublisher busPublisher)
            : base(repository, busPublisher)
        {
        }

        protected override Task<WarehouseEntity> Complete(WarehouseUpsertCommand command, WarehouseEntity entity, CancellationToken cancellationToken)
        {
            if (entity is null)
                entity = new WarehouseEntity();

            entity.SetProperty(e => e.BookId, command.BookId);
            entity.SetProperty(e => e.Quantity, command.Quantity);
            entity.SetProperty(e => e.Price, command.Price);

            if (entity.IsTransient)
                entity.AddDomainEvent(new WarehouseCreatedDomainEvent(entity));
            else if (entity.IsDirty)
                entity.AddDomainEvent(new WarehouseUpdatedDomainEvent(entity));

            return Task.FromResult(entity);
        }
    }
}
