﻿using Book.Application.Commands;
using Book.Application.Queries;
using Book.Application.ViewModels;
using Book.Domain.Entities;
using eBook.Base.API.Controllers;
using eBook.Base.Common.Mvc;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using OpenTracing;
using System.Collections.Generic;

namespace Book.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : CqrsApiControllerBase<BookEntity, BookDTO>
    {
        public BookController(IMediator mediator, ITracer tracer, IServiceId serviceId)
            : base(mediator, tracer, serviceId)
        {
        }

        protected override IRequest UpsertCommand(BookDTO dto)
        {
            return new BookUpsertCommand()
            {
                Id = dto.Id,
                Author = dto.Author,
                ISBN = dto.ISBN,
                Title = dto.Title,
                Price = dto.Price
            };
        }

        protected override IRequest DeleteCommand(int entityId)
        {
            return new BookDeleteCommand() { Id = entityId };
        }

        protected override IRequest<IEnumerable<BookDTO>> FetchListCommand()
        {
            return new BookListQuery();
        }

        protected override IRequest<BookDTO> FetchSingleCommand(int entityId)
        {
            return new BookSingleQuery() { EntityId = entityId };
        }
    }
}