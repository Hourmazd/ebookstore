﻿using AutoMapper;
using Book.Application.ViewModels;
using Book.Domain.Entities;
using Book.Persistence.Repositories;
using eBook.Base.Application.Queries;
using eBook.Base.Common.RabbitMq;

namespace Book.Application.Queries
{
    public class BookSingleQuery : SingleQueryBase<BookDTO>
    {
        public BookSingleQuery(ICorrelationContext context = null)
            : base(context) { }
    }

    public class BookSingleQueryHandler
        : SingleQueryHandlerBase<BookEntity, BookDTO, IBookRepository, BookSingleQuery>
    {
        public BookSingleQueryHandler(IBookRepository repository, IMapper mapper)
            : base(repository, mapper)
        {
        }
    }
}
