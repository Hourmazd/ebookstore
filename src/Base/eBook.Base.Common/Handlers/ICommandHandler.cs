﻿using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Types;
using System.Threading.Tasks;

namespace eBook.Base.Common.Handlers
{
    public interface ICommandHandler<in TCommand>
        where TCommand : ICommand
    {
        Task HandleAsync(TCommand command, ICorrelationContext context);
    }
}
