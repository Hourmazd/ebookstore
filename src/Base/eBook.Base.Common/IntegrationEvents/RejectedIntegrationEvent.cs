﻿using Newtonsoft.Json;
using System;

namespace eBook.Base.Common.IntegrationEvents
{
    public class RejectedIntegrationEvent : IntegrationEvent, IRejectedIntegrationEvent
    {
        public string Reason { get; }
        public string Code { get; }

        [JsonConstructor]
        public RejectedIntegrationEvent(string reason, string code) : base()
        {
            Reason = reason;
            Code = code;
        }

        [JsonConstructor]
        public RejectedIntegrationEvent(Guid id, string reason, string code) : base(id)
        {
            Reason = reason;
            Code = code;
        }

        public static IRejectedIntegrationEvent For(string name)
            => new RejectedIntegrationEvent($"There was an error when executing: " + $"{name}", $"{name}_error");
    }
}
