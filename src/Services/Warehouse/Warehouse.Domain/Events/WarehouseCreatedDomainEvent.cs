﻿using Warehouse.Domain.Entities;
using eBook.Base.Domain.Events;

namespace Warehouse.Domain.Events
{
    public class WarehouseCreatedDomainEvent : DomainEventBase<WarehouseEntity>
    {
        public WarehouseCreatedDomainEvent(WarehouseEntity entity) : base(entity)
        {
        }
    }
}
