﻿using Book.Domain.Events;
using Book.Persistence.Repositories;
using MediatR;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace Book.Domain.EventHandlers
{
    public class BookUpdatedDomainEventHandler : INotificationHandler<BookUpdatedDomainEvent>
    {
        private readonly ILogger<BookUpdatedDomainEventHandler> _Logger;
        private readonly IBookRepository _Repository;

        public BookUpdatedDomainEventHandler(
            ILogger<BookUpdatedDomainEventHandler> logger,
            IBookRepository repository)
        {
            _Logger = logger;
            _Repository = repository;
        }

        public Task Handle(BookUpdatedDomainEvent notification, CancellationToken cancellationToken)
        {
            _Logger.LogInformation($"\t>> Book {notification.EntityId} has been updated.");

            return Task.CompletedTask;
        }
    }
}
