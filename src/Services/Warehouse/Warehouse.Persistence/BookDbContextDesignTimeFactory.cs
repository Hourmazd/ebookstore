﻿using eBook.Base.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Warehouse.Persistence
{
    public class BookDbContextDesignTimeFactory : DbContextDesignTimeFactoryBase<WarehouseDbContext>
    {
        protected override string ConnectionStringName => "Sqlite";
        protected override string ProjectName => "Warehouse.API";
        protected override string AspNetCoreEnvironment => "Development";

        protected override WarehouseDbContext CreateNewInstance(string connectionString)
        {
            var options = new DbContextOptionsBuilder<WarehouseDbContext>()
                .UseSqlite(connectionString);
                //.UseSqlServer(connectionString);

            return new WarehouseDbContext(options.Options);
        }
    }
}
