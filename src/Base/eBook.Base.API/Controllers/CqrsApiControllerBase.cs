﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using eBook.Base.Domain.Entities;
using eBook.Base.Application.ViewModels;
using System;
using eBook.Base.Common.Types;
using OpenTracing;
using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Mvc;

namespace eBook.Base.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class CqrsApiControllerBase<TEntity, TDTO> : ApiControllerBase
        where TEntity : EntityBase
        where TDTO : DTOBase
    {
        protected IMediator Mediator { get; }

        protected CqrsApiControllerBase(IMediator mediator, ITracer tracer, IServiceId serviceId)
            : base(tracer, serviceId)
        {
            Mediator = mediator;
        }

        protected abstract IRequest<IEnumerable<TDTO>> FetchListCommand();

        protected abstract IRequest<TDTO> FetchSingleCommand(int entityId);

        protected abstract IRequest DeleteCommand(int entityId);

        protected abstract IRequest UpsertCommand(TDTO dto);

        [Route("[action]")]
        [HttpGet]
        public Task<ActionResult<IEnumerable<TDTO>>> Get(CancellationToken cancellationToken)
        {
            return FetchList(FetchListCommand() as ICommand, cancellationToken);
        }

        [Route("[action]/{entityId:int}")]
        [HttpGet]
        public Task<ActionResult<TDTO>> Get(int entityId, CancellationToken cancellationToken)
        {
            return FetchSingle(FetchSingleCommand(entityId) as ICommand, cancellationToken);
        }

        [Route("[action]")]
        [HttpPost]
        public Task<IActionResult> Upsert(TDTO dto, CancellationToken cancellationToken)
        {
            return SendAsync(UpsertCommand(dto) as ICommand, cancellationToken);
        }

        [Route("[action]/{entityId:int}")]
        [HttpDelete]
        public Task<IActionResult> Delete(int entityId, CancellationToken cancellationToken)
        {
            return SendAsync(DeleteCommand(entityId) as ICommand, cancellationToken);
        }

        protected async Task<IActionResult> SendAsync<T>(
            T command,
            CancellationToken cancellationToken,
            Guid? resourceId = null,
            string resource = "")
            where T : ICommand
        {
            if (command.Context is null)
                command.SetContext(GetContext<T>(resourceId, resource));

            await Mediator.Send(command, cancellationToken);
            return Accepted(command.Context);
        }

        protected IActionResult Accepted(ICorrelationContext context)
        {
            Response.Headers.Add(OperationHeader, $"operations/{context.Id}");
            if (!string.IsNullOrWhiteSpace(context.Resource))
            {
                Response.Headers.Add(ResourceHeader, context.Resource);
            }

            return base.Accepted();
        }

        protected async Task<ActionResult<TDTO>> FetchSingle<T>(
            T command,
            CancellationToken cancellationToken,
            Guid? resourceId = null,
            string resource = "")
            where T : ICommand
        {
            if (command.Context is null)
                command.SetContext(GetContext<T>(resourceId, resource));

            var result = await Mediator.Send(command, cancellationToken);

            if (result is null)
                return NotFound();
            else
                return Ok(result);
        }

        protected async Task<ActionResult<IEnumerable<TDTO>>> FetchList<T>(
            T command,
            CancellationToken cancellationToken,
            Guid? resourceId = null,
            string resource = "")
            where T : ICommand
        {
            if (command.Context is null)
                command.SetContext(GetContext<T>(resourceId, resource));

            var result = await Mediator.Send(command, cancellationToken);

            if (result is null)
                return NotFound();
            else
                return Ok(result);
        }
    }
}
