﻿using Warehouse.Application.ViewModels;
using RestEase;
using System.Threading.Tasks;

namespace Warehouse.Application.Forwarders
{
    public interface IBookForwarder
    {
        [AllowAnyStatusCode]
        [Get("api/book/get/{id}")]
        Task<BookDTO> GetBook([Path] int id);
    }
}
