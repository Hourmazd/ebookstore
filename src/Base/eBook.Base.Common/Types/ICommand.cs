﻿using eBook.Base.Common.RabbitMq;

namespace eBook.Base.Common.Types
{
    public interface ICommand
    {
        ICorrelationContext Context { get; }

        void SetContext(ICorrelationContext context);
    }
}
