﻿using eBook.Base.Common.IntegrationEvents;
using eBook.Base.Common.Types;
using System;

namespace Warehouse.Application.IntegrationEvents.Events
{
    [MessageNamespace("bookService")]
    public class BookPriceChangedRejectedEvent : RejectedIntegrationEvent
    {
        public int BookId { get; }

        public BookPriceChangedRejectedEvent(Guid eventId, int bookId, string reason, string code)
            :base(eventId, reason, code)
        {
            BookId = bookId;
        }
    }
}
