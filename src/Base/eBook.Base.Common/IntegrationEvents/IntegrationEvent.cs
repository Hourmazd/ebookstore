﻿using Newtonsoft.Json;
using System;

namespace eBook.Base.Common.IntegrationEvents
{
    public abstract class IntegrationEvent : IIntegrationEvent
    {
        public IntegrationEvent(Guid id)
        {
            Id = id;
            CreationDate = DateTime.UtcNow;
        }

        public IntegrationEvent()
            : this(Guid.NewGuid())
        {
        }

        [JsonConstructor]
        public IntegrationEvent(Guid id, DateTime createDate)
        {
            Id = id;
            CreationDate = createDate;
        }

        [JsonProperty]
        public Guid Id { get; private set; }

        [JsonProperty]
        public DateTime CreationDate { get; private set; }
    }
}
