﻿using Book.Domain.Entities;
using Book.Domain.Events;
using Book.Persistence.Repositories;
using eBook.Base.Application.Commands;
using eBook.Base.Common.RabbitMq;
using System.Threading;
using System.Threading.Tasks;

namespace Book.Application.Commands
{
    public class BookDeleteCommand : DeleteCommandBase
    {
        public BookDeleteCommand(ICorrelationContext context = null)
            : base(context) { }
    }

    public class BookDeleteCommandHandler
        : DeleteCommandHandlerBase<BookEntity, BookDeleteCommand, IBookRepository>
    {
        public BookDeleteCommandHandler(IBookRepository repository, IBusPublisher busPublisher)
            : base(repository, busPublisher)
        {
        }

        protected override Task<BookEntity> Complete(BookDeleteCommand command, BookEntity entity, CancellationToken cancellationToken)
        {
            if (entity != null)
                entity.AddDomainEvent(new BookDeletedDomainEvent(command.Id));

            return Task.FromResult(entity);
        }
    }
}
