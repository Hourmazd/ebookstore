﻿using eBook.Base.Common.Consul;
using eBook.Base.Common.HealthCheck;
using eBook.Base.Common.Jeager;
using eBook.Base.Common.Mvc;
using eBook.Base.Common.Redis;
using eBook.Base.Common.Swagger;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace eBook.Base.API.Extentions
{
    public static class Extentions
    {
        public static IServiceCollection AddDefaultServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDefaultHealthCheck(configuration);
            services.AddCustomMvc(configuration);
            services.AddSwaggerDocs();
            services.AddConsul();
            services.AddJaeger();
            services.AddRedis();
            services.AddMetrics();

            return services;
        }

        public static IApplicationBuilder UseDefaultApplicationSettings(
            this IApplicationBuilder builder,
            bool useAuthorization = false,
            bool useAuthentication = false)
        {
            builder.UseAllForwardedHeaders();
            builder.UseSwaggerDocs();
            builder.UseErrorHandler();
            builder.UseServiceId();
            builder.UseConsul();
            //builder.UseRabbitMq();

            if (useAuthentication)
                builder.UseAuthentication();

            if (useAuthorization)
                builder.UseAuthorization();

            builder.UseRouting();

            builder.UseDefaultEndpoints();

            return builder;
        }
    }
}
