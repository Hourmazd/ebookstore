﻿using Gateway.Application.ViewModels;
using RestEase;
using System.Threading.Tasks;

namespace Gateway.Application.Forwarders
{
    public interface IBookForwarder
    {
        [AllowAnyStatusCode]
        [Get("api/book/get/{id}")]
        Task<BookDTO> Get([Path] int id);

        [AllowAnyStatusCode]
        [Post("api/book/upsert")]
        Task<object> Upsert([Body]BookDTO book);
    }
}
