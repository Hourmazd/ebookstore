﻿using eBook.Base.Domain.Events;

namespace Warehouse.Domain.Events
{
    public class WarehouseDeletedDomainEvent : DomainEventBase
    {
        public WarehouseDeletedDomainEvent(int warehouseId) : base(warehouseId)
        {
        }
    }
}
