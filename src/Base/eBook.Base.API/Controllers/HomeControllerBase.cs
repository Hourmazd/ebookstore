using eBook.Base.Common;
using eBook.Base.Common.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace eBook.Base.API.Controllers
{
    [Route("")]
    [ApiController]
    public abstract class HomeControllerBase : ControllerBase
    {
        protected AppOptions _AppOptions;
        protected IServiceId _ServiceId;

        protected HomeControllerBase(IOptions<AppOptions> options, IServiceId serviceId)
        {
            _AppOptions = options.Value;
            _ServiceId = serviceId;
        }

        [HttpGet]
        public ActionResult<string> Get() => Ok($"API SERVICE >> {_AppOptions.Name}, Version: {_AppOptions.Version}, Id: {_ServiceId.Id}");
    }
}