﻿using eBook.Base.Common.RabbitMq;
using eBook.Base.Common.Types;
using eBook.Base.Domain.Entities;
using eBook.Base.Persistence.Repositories;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace eBook.Base.Application.Commands
{
    public abstract class DeleteCommandBase : IRequest, ICommand
    {
        public ICorrelationContext Context { get; set; }

        public int Id { get; set; }

        protected DeleteCommandBase (ICorrelationContext context)
        {
            Context = context;
        }

        public void SetContext(ICorrelationContext context)
        {
            Context = context;
        }
    }

    public abstract class DeleteCommandHandlerBase<TEntity, TCommand, TRepository> : IRequestHandler<TCommand>
        where TEntity : EntityBase
        where TCommand : DeleteCommandBase
        where TRepository : IRepositoryBase<TEntity>
    {
        protected readonly TRepository _Repository;
        protected readonly IBusPublisher _BusPublisher;

        protected DeleteCommandHandlerBase(TRepository repository, IBusPublisher busPublisher)
        {
            _Repository = repository;
            _BusPublisher = busPublisher;
        }

        public async Task<Unit> Handle(TCommand request, CancellationToken cancellationToken)
        {
            var entity = await _Repository.GetEntityById(request.Id);

            entity = await Complete(request, entity, cancellationToken);

            await _Repository.Remove(entity, cancellationToken);

            return Unit.Value;
        }

        protected abstract Task<TEntity> Complete(TCommand command, TEntity entity, CancellationToken cancellationToken);

    }
}
