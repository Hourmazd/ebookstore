﻿using eBook.Base.Common.IntegrationEvents;
using System.Threading.Tasks;
using eBook.Base.Common.RabbitMq;

namespace eBook.Base.Common.Handlers
{
    public interface IEventHandler<in TEvent> 
        where TEvent : IIntegrationEvent
    {
        Task HandleAsync(TEvent @event, ICorrelationContext context);
    }
}
