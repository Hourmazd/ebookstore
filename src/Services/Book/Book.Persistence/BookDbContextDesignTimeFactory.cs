﻿using eBook.Base.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Book.Persistence
{
    public class BookDbContextDesignTimeFactory : DbContextDesignTimeFactoryBase<BookDbContext>
    {
        protected override string ConnectionStringName => "Sqlite";
        protected override string ProjectName => "Book.API";
        protected override string AspNetCoreEnvironment => "Development";

        protected override BookDbContext CreateNewInstance(string connectionString)
        {
            var options = new DbContextOptionsBuilder<BookDbContext>()
                .UseSqlite(connectionString);
                //.UseSqlServer(connectionString);

            return new BookDbContext(options.Options);
        }
    }
}
